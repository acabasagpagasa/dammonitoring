<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>View Record</title>
<!--   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css"> -->
  <link rel="stylesheet" href="{{asset('css/app.css')}}">

  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


  <body>
<form method="POST" action="{{url('/save')}}" id="myform">
{{csrf_field()}}
  <nav class="navbar navbar-inverse vertical-center">
    <div class="container-fluid">
      <div class="pull-right">
        <a class="btn btn-success navbar-btn" href="/lists/" role="button">OK</a>
      </div>
      <div class="pull-left">
        <a class="btn btn-danger navbar-btn" href="/lists/" role="button">Home</a>
      </div>
    </div>
  </nav>
  <div class="container-fluid">
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
      <div class="table-responsive">
        <div class="col-md-12 table-responsive">      
          <div class="row">
            <div class="col-md-4">
              <label>Date & Time Updated: &nbsp </label>
              <input type="text"  size="90" value=" {{$viewRec[0]->observation_datetime}}" class="form-control" disabled="disabled" />
            </div>
            <div class="col-md-4">
              <label>&nbsp  &nbsp  &nbspPrepared by:  &nbsp</label>
              <input type="text" name='prepared_by[]' size="90" value=" {{$viewRec[0]->prepared_by}}" class="form-control"  disabled="disabled" />
            </div>
          
          </div> 
          <br>
          <br>
          <br>
          <br>
        <table class="table table-bordered table-hover table-sortable" id="tab_logic">
          <thead>
            <tr >
              <th class="text-center" width="130">
                Dam Name
              </th>
              <th class="text-center"  width="180">
                Observation Time & Date
              </th>
              <th class="text-center">
                Reservoir Water Level (RWL) (m)
              </th>
              <th class="text-center">
                Water Level Deviation
              </th>
              <th class="text-center">
                Normal High Water Level (NHWL) (m)
              </th>
              <th class="text-center">
                Deviation from NHWL (m)
              </th>
              <th class="text-center">
                Rule Curve Elevation (m)
              </th>
              <th class="text-center">
                Deviation from Rule Curve (m)
              </th>
              <th class="text-center">
                Estimated RR Amount to reach NHWL(mm)
              </th>
              <th class="text-center">
                Dam Release
              </th>
            </tr>
          </thead>
          <tbody>
              <tr id='addr0' data-id="0" >

            <?php $ctr = 1;  ?>
            @foreach ($viewRec as $arrays )

                <td name='damname' >
                  <input type="text" name='DamName[]' value="{{$arrays->damName}}" class="form-control" disabled="disabled"/>
                </td>
              <td name="obserDT">
                <input type="text" name='observation_datetime[]' value="{{$arrays->observation_datetime}}"  class="form-control" disabled="disabled" />
              </td>
              <td name="RWL">
                <input type="number" step=".01" name='waterlevel_m[]' value="{{$arrays->waterlevel_m}}"  class="form-control" disabled="disabled"/>
              </td>
              <td name="24HRDev">
                  <input type="number" name='waterlevel_deviation_m[]' value="{{$arrays->waterlevel_deviation_m}}"   class="form-control" disabled="disabled"/>
              </td>
              <td name="NHWL">
                  <input type="number" step=".01" name='norwal_high_water_level[]' value="{{$arrays->norwal_high_water_level}}"   class="form-control" disabled="disabled"/>
              </td>
              <td name="devFrNHWL">
                  <input type="number" step=".01" name='deviation_fr_nhwl[]' value="{{$arrays->deviation_fr_nhwl}}"   class="form-control" disabled="disabled"/>
              </td>
              <td name="ruleCurlElev">
                  <input type="number" step=".01" name='rule_curve_elevation[]' value="{{$arrays->rule_curve_elevation}}"   class="form-control" disabled="disabled"/>
              </td>
              <td name="devFrRuleC">
                  <input type="number" step=".01" name='deviation_from_rule_curve[]' value="{{$arrays->deviation_from_rule_curve}}"   class="form-control" disabled="disabled"/>
              </td>
              <td name="estRRAmt">
                  <input type="number" step=".01" name='estimated_rr_to_reach_nhwl[]' value="{{$arrays->estimated_rr_to_reach_nhwl}}"  class="form-control" disabled="disabled" />
              </td>
              <td name="gateOpen">
                  <input type="text"  name='gate_opening[]' value="{{$arrays->gate_opening}}"  class="form-control" id="gate_opening{{$ctr}}" disabled="disabled" />
              </td>
            </tr>        

          <?php $ctr = $ctr + 1 ?>
          @endforeach
          </tbody>
        </table>
        <br>
        <br>
        <table align="center">
          <tr>
            <td>Trend for the past 24 hours:</td>
            <td> &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp</td>
            <td>Data Source: Dam Operating/Managing Agencies</td>
          </tr>
          <tr>
            <td>+ Deviation indicates increase from previous WL</td>
          </tr>
          <tr>
            <td>- Deviation indicates decrease from previous WL</td>
          </tr>
        </table>
          <br>
        <br>

      </div>
    </div>
  </div>
</form>
</body>
</html>