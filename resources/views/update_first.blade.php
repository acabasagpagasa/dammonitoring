<!DOCTYPE html>

<html>
<head>
  <meta charset="utf-8">
  <title>Update Record</title>
<!--   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css"> -->
  <link rel="stylesheet" href="{{asset('css/app.css')}}">

  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  <script type="text/javascript">
    $(function(){

        $("#date_updated").on("change", function(){
          $( "input[id^='observation_date']" ).val(this.value);
        });

        $("#time_updated").on("change", function(){
          $( "input[id^='observation_time']" ).val(this.value);
        });
 
        $("#waterlevel_m_new0").on("change", function(){
            $("#deviation0").val(compute(this.value,this.id.slice(-1)));
        });           
      
        $("#waterlevel_m_new1").on("change", function(){
            $("#deviation1").val(compute(this.value,this.id.slice(-1)));
        }); 

        $("#waterlevel_m_new2").on("change", function(){
            $("#deviation2").val(compute(this.value,this.id.slice(-1)));
        });           
      
        $("#waterlevel_m_new3").on("change", function(){
            $("#deviation3").val(compute(this.value,this.id.slice(-1)));
        }); 

        $("#waterlevel_m_new4").on("change", function(){
            $("#deviation4").val(compute(this.value,this.id.slice(-1)));
        });           
      
        $("#waterlevel_m_new5").on("change", function(){
            $("#deviation5").val(compute(this.value,this.id.slice(-1)));
        }); 

        $("#waterlevel_m_new6").on("change", function(){
            $("#deviation6").val(compute(this.value,this.id.slice(-1)));
        });           
      
        $("#waterlevel_m_new7").on("change", function(){
            $("#deviation7").val(compute(this.value,this.id.slice(-1)));
        }); 

        $("#waterlevel_m_new8").on("change", function(){
            $("#deviation8").val(compute(this.value,this.id.slice(-1)));
        });


    });

      function compute(m,r){
        var waterlevel_m_new;
        var waterlevel_m_old;
        var deviation;

        waterlevel_m_new = m;

        if (r == "0") {
          waterlevel_m_old = 0;
        }
        else if (r == "1") {
          waterlevel_m_old = 0;
        }
        else if (r == "2") {
          waterlevel_m_old = 0;
        }
        else if (r == "3") {
          waterlevel_m_old = 0;
        }       
        else if (r == "4") {
          waterlevel_m_old = 0;
        }
        else if (r == "5") {
          waterlevel_m_old = 0;
        }
        else if (r == "6") {
          waterlevel_m_old = 0;
        }
        else if (r == "7") {
          waterlevel_m_old = 0;
        }
        else  {
          waterlevel_m_old = 0;
        }

        deviation = (waterlevel_m_new - waterlevel_m_old); 
        deviation = Math.round(deviation * 100) / 100
        return deviation ;
    };  
  </script>
</head>

<body>
<form method="POST" action="{{url('/update')}}" id="myform">
{{csrf_field()}}
  <nav class="navbar navbar-inverse vertical-center">
    <div class="container-fluid">
      <div class="pull-right">
        <button class="btn btn-warning navbar-btn" type="submit" id="submit">++ Save Record</button>
      </div>
      <div class="pull-left">
        <a class="btn btn-danger navbar-btn" href="/lists/" role="button">Home</a>
      </div>
    </div>
  </nav>
  <div class="container-fluid">
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
      <div class="table-responsive">
        <div class="col-md-12 table-responsive">

        
            <?php 
              $tmp = explode(' ', $d2[0]->observation_datetime);
              $datenow = $tmp[0];
              $timenow = date('h:s A',strtotime($tmp[1]));

            ?>

          <div class="row">
            <div class="col-md-4">
              <label>Date Updated: &nbsp </label>
              <input type="text" name='date_updated' id="date_updated" class="form-control" value="{{$datenow}}" required readonly />
            </div>
            <div class="col-md-4">
              <label>&nbsp  &nbsp  &nbsp Time Updated:  &nbsp</label>
              <select type="time" name='time_updated' id='time_updated' class="form-control"  required>
                <option selected="selected" value="{{$timenow}}">{{$timenow}} </option>
                @foreach($time as $t)
                 <option value="{{$t}}">{{$t}}</option>
                @endforeach
              </select>
              </div>
            <div class="col-md-4">
              <label>&nbsp  &nbsp  &nbspPrepared by:  &nbsp</label>
              <input type="text" name='prepared_by' size="90" class="form-control" value="{{$d2[0]->prepared_by}}" required />
            </div>
          </div>        
          <br>
          <br>
          <br>
          <br>
        <table class="table table-bordered table-hover table-sortable" id="tab_logic">
          <thead>
            <tr >
              <th class="text-center" width="130">
                Dam Name
              </th>
              <th class="text-center"  width="200">
                Observation Time & Date
              </th>
              <th class="text-center">
                Reservoir Water Level (RWL) (m)
              </th>
              <th class="text-center">
                Water Level Deviation
              </th>
              <th class="text-center">
                Normal High Water Level (NHWL) (m)
              </th>
              <th class="text-center">
                Deviation from NHWL (m)
              </th>
              <th class="text-center">
                Rule Curve Elevation (m)
              </th>
              <th class="text-center">
                Deviation from Rule Curve (m)
              </th>
              <th class="text-center">
                Estimated RR Amount to reach NHWL(mm)
              </th>
              <th class="text-center">
                Dam Release
              </th>
            </tr>
          </thead>
          <tbody>
              <tr id='addr0' data-id="0" >

              <?php $ctr = 1;  $ctr2 = 0;?>
                dd($d2);

            @foreach ($d2 as $arrays )

                <td name='ID' hidden>
                  <input type="text" name='dam_id_mon[]' value="{{$arrays->id}}" class="form-control"/>
                </td>
                <td name='damname' >
                  <input type="text" name='DamName' readonly value="{{$arrays->damName}}" class="form-control"/>
                </td>
              <td name="obserDT">
                <input type="text" name='observation_date[]'  readonly class="form-control" id="observation_date{{ $ctr }}" value="{{$datenow}}" />
                <input type="text" name='observation_time[]' readonly  class="form-control" id="observation_time{{ $ctr }}" value="{{$timenow}}"/>
              </td>
              <td name="RWL">
                <input type="number" step=".01" name='waterlevel_m[]' id= "waterlevel_m_new{{$ctr2}}"  class="form-control" value="{{$arrays->waterlevel_m}}"/>
              </td>
              <td name="24HRDev">
                  <input type="number" name='waterlevel_deviation_m[]' id="deviation{{$ctr2}}"  readonly class="form-control" value="{{$arrays->waterlevel_deviation_m}}"/>
              </td>
              <td name="NHWL">
                  <input type="number" step=".01" name='norwal_high_water_level[]'  class="form-control" value="{{$arrays->norwal_high_water_level}}"/>
              </td>
              <td name="devFrNHWL">
                  <input type="number" step=".01" name='deviation_fr_nhwl[]'  class="form-control" value="{{$arrays->deviation_fr_nhwl}}"/>
              </td>
              <td name="ruleCurlElev">
                  <input type="number" step=".01" name='rule_curve_elevation[]'  class="form-control" value="{{$arrays->rule_curve_elevation}}"/>
              </td>
              <td name="devFrRuleC">
                  <input type="number" step=".01" name='deviation_from_rule_curve[]'  class="form-control" value="{{$arrays->deviation_from_rule_curve}}"/>
              </td>
              <td name="estRRAmt">
                  <input type="number" step=".01" name='estimated_rr_to_reach_nhwl[]'  class="form-control" value="{{$arrays->estimated_rr_to_reach_nhwl}}"/>
              </td>
              <td name="gateOpen">
                  <input type="text"  name='gate_opening[]'  class="form-control" value="{{$arrays->gate_opening}}"/>
              </td>
            </tr>

            <?php $ctr2 = $ctr2 + 1 ?>

            <?php $ctr = $ctr + 1 ?>

          @endforeach
          </tbody>
        </table>
        <br>
        <br>
        <table align="center">
          <tr>
            <td>Trend for the past 24 hours:</td>
            <td> &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp</td>
            <td>Data Source: Dam Operating/Managing Agencies</td>
          </tr>
          <tr>
            <td>+ Deviation indicates increase from previous WL</td>
          </tr>
          <tr>
            <td>- Deviation indicates decrease from previous WL</td>
          </tr>
        </table>
          <br>
        <br>

      </div>
    </div>
  </div>
</form>

</body>


</html>