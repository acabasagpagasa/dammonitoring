<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Login</title>
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css"> -->
  <link rel="stylesheet" href="{{asset('css/app.css')}}">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
</head>
<body>
<form method="POST" action="{{url('login/a')}}" accept-charset="UTF-8">
<br>
<br>
	<div class="container">
	<div class="span4 offset4 well" >
	    <div class="row">  
			<h3> &nbsp &nbspSign In</h3>
			<div class="col-md-4">
				<input type="text" id="username" name="username" placeholder="Username" class="form-control"/>
			</div>
			<div class="col-md-4">
				<input type="password" id="password" name="password" placeholder="Password" class="form-control"/>
			</div>
			<div class="col-md-4">
				<button type="submit" name="submit" class="btn btn-info ">Sign in</button>
			</div>
		</div>
	</div>
	</div>
</form>
</body>
</html>