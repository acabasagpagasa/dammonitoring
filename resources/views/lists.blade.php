<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Home</title>
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css"> -->
  <link rel="stylesheet" href="{{asset('css/app.css')}}">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
</head>

<body>

  <nav class="navbar navbar-inverse vertical-center">
    <div class="container-fluid">
      <div class="pull-right">
        <a class="btn btn-success navbar-btn" href="/create/" role="button">+ Create New</a>
      </div>
    </div>
  </nav>

  <div class="container  ">
    <br>
    <br>
    <br>


    <div class="row">
    @if(Session::has('success'))
      <p class="bg-success" style="padding:15px">
        {{Session::get('success')}}
      </p>
      <br>
      <br>
      <br>
    @endif

    @if(Session::has('update'))
      <p class="bg-success" style="padding:15px">
        {{Session::get('update')}}
      </p>
      <br>
      <br>
      <br>
    @endif

    @if(Session::has('create'))
      <p class="bg-success" style="padding:15px">
        {{Session::get('create')}}
      </p>
      <br>
      <br>
      <br>
    @endif

    <form method="POST" action="{{url('dams/delete')}}">
      {{csrf_field()}}
      <input type="hidden" name="date_time" value="{{isset($data[0]) ? $data[0]->observation_datetime : ''}}">
      <?php $ctr = 0; ?>

          @foreach ($data as $arrays )

            <div class="col-xs-9">
              <li class="list-group-item">{{$arrays->observation_datetime}}</li>
            </div>
            <div class="col-xs-6 col-sm-3 ">
              <a href='/dams/view/{{$arrays->observation_datetime}}' data-date="{{$arrays->observation_datetime}}" class="btn btn-info vButton " role="button" id="view">View</a>
            
            <?php 
              $tmp = explode(' ', $arrays->observation_datetime);
              $datenow = $tmp[0];
            ?>

            @if ($ctr == 0)
              @if ( Carbon\carbon::today()->format('Y-m-d') == $datenow )
                <a href='/dams/update/{{$arrays->observation_datetime}}' data-date="{{$arrays->observation_datetime}}" class="btn btn-warning aButton " role="button" id="update">Update</a>
                <input type="submit" class="btn btn-danger eButton" value="Delete" id="delete" >

              @endif
           
            @endif

            </div>

          <?php $ctr = $ctr + 1 ?>
          @endforeach
      </form>
    </div>
  </div>

  
</body>


</html>
<script type="text/javascript">
$(function(){

  $('.vButton').click(function(){
    var date = $(this).data().date;
    $.get('view/'+date,function(a){
      console.log(a);
    });

  });

  $('.aButton').click(function(){
    var date = $(this).data().date;
    $.get('update/'+date,function(a){
      console.log(a);
    });
  });

  $('.eButton').click(function(e){
    var date = $(this).data().date;
    if(!confirm('Are you sure you want to delete this?')){
            e.preventDefault();
            return false;
        }else{
            $.get('delete/'+date,function(a){});
          }
    });
});


</script>