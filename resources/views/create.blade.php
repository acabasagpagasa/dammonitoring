<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Create New</title>
<!--   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css"> -->
  <link rel="stylesheet" href="{{asset('css/app.css')}}">

  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  <script type="text/javascript">

    $(function(){
        $("#date_updated").datepicker();
        $("#date_updated").datepicker().datepicker("setDate", new Date());
        var temptime = $("#date_updated").val();
        $( "input[id^='observation_date']" ).val(temptime);      

        var temp="06:00:00 AM"; 
        var temp1="06:00 AM";
        $("#time_updated").val(temp1);
        $( "input[id^='observation_time']" ).val(temp);

        $("#date_updated").on("change", function(){
          $( "input[id^='observation_date']" ).val(this.value);
        });

        $("#time_updated").on("change", function(){
          $( "input[id^='observation_time']" ).val(this.value);
        });
 
        $("#waterlevel_m_new0").on("change", function(){
           $("#deviation0").val(compute(this.value,this.id.slice(-1)));
          
            
        });           
      
        $("#waterlevel_m_new1").on("change", function(){
            $("#deviation1").val(compute(this.value,this.id.slice(-1)));
        }); 

        $("#waterlevel_m_new2").on("change", function(){
            $("#deviation2").val(compute(this.value,this.id.slice(-1)));
        });           
      
        $("#waterlevel_m_new3").on("change", function(){
            $("#deviation3").val(compute(this.value,this.id.slice(-1)));
        }); 

        $("#waterlevel_m_new4").on("change", function(){
            $("#deviation4").val(compute(this.value,this.id.slice(-1)));
        });           
      
        $("#waterlevel_m_new5").on("change", function(){
            $("#deviation5").val(compute(this.value,this.id.slice(-1)));
        }); 

        $("#waterlevel_m_new6").on("change", function(){
            $("#deviation6").val(compute(this.value,this.id.slice(-1)));
        });           
      
        $("#waterlevel_m_new7").on("change", function(){
            $("#deviation7").val(compute(this.value,this.id.slice(-1)));
        }); 

        $("#waterlevel_m_new8").on("change", function(){
            $("#deviation8").val(compute(this.value,this.id.slice(-1)));
        });


    });

      function compute(m,r){
        var waterlevel_m_new;
        var waterlevel_m_old;
        var deviation;

        waterlevel_m_new = m;

        if (r == "0") {
          waterlevel_m_old = parseFloat($('#waterlevel_m_old0').val());
        }
        else if (r == "1") {
          waterlevel_m_old = parseFloat($('#waterlevel_m_old1').val());
        }
        else if (r == "2") {
          waterlevel_m_old = parseFloat($('#waterlevel_m_old2').val());
        }
        else if (r == "3") {
          waterlevel_m_old = parseFloat($('#waterlevel_m_old3').val());
        }       
        else if (r == "4") {
          waterlevel_m_old = parseFloat($('#waterlevel_m_old4').val());
        }
        else if (r == "5") {
          waterlevel_m_old = parseFloat($('#waterlevel_m_old5').val());
        }
        else if (r == "6") {
          waterlevel_m_old = parseFloat($('#waterlevel_m_old6').val());
        }
        else if (r == "7") {
          waterlevel_m_old = parseFloat($('#waterlevel_m_old7').val());
        }
        else  {
          waterlevel_m_old = parseFloat($('#waterlevel_m_old8').val());
        }

        if (isNaN(waterlevel_m_old)) {
          waterlevel_m_old = 0;
        }

        deviation = (waterlevel_m_new - waterlevel_m_old); 
        deviation = Math.round(deviation * 100) / 100
        return deviation ;
    };  
  </script>
</head>

<body>
<form method="POST" action="{{url('/save')}}" id="myform">
{{csrf_field()}}
  <nav class="navbar navbar-inverse vertical-center">
    <div class="container-fluid">
      <div class="pull-right">
        <button class="btn btn-success navbar-btn" type="submit" id="submit">+ Save</button>
      </div>
      <div class="pull-left">
        <a class="btn btn-danger navbar-btn" href="/lists/" role="button">Home</a>
      </div>
    </div>
  </nav>
  <div class="container-fluid">
  <br>
  <br>
  <br>
  <br>
  <br>
  <br>
      <div class="table-responsive">
        <div class="col-md-12 table-responsive">
          <div class="row">
            <div class="col-md-4">
              <label>Date Updated: &nbsp </label>
              <input type="text" name='date_updated[]' id="date_updated" class="form-control" required />
            </div>
            <div class="col-md-4">
              <label>&nbsp  &nbsp  &nbsp Time Updated:  &nbsp</label>
              <select type="time" name='time_updated[]' id='time_updated' class="form-control" required>
                <option value="">--SELECT--</option>
                @foreach($time as $t)
                 <option value="{{$t}}">{{$t}}</option>
                @endforeach
              </select>
              </div>
            <div class="col-md-4">
              <label>&nbsp  &nbsp  &nbspPrepared by:  &nbsp</label>
              <input type="text" name='prepared_by[]' size="90" class="form-control" required />
            </div>
          </div>        
          <br>
          <br>
          <br>
          <br>
        <table class="table table-bordered table-hover table-sortable" id="tab_logic">
          <thead>
            <tr >
              <th class="text-center" width="130">
                Dam Name
              </th>
              <th class="text-center"  width="200">
                Observation Time & Date
              </th>
              <th class="text-center">
                Reservoir Water Level (RWL) (m)
              </th>
              <th class="text-center">
                Water Level Deviation
              </th>
              <th class="text-center">
                Normal High Water Level (NHWL) (m)
              </th>
              <th class="text-center">
                Deviation from NHWL (m)
              </th>
              <th class="text-center">
                Rule Curve Elevation (m)
              </th>
              <th class="text-center">
                Deviation from Rule Curve (m)
              </th>
              <th class="text-center">
                Estimated RR Amount to reach NHWL(mm)
              </th>
              <th class="text-center">
                Dam Release
              </th>
            </tr>
          </thead>
          <tbody>
              <tr id='addr0' data-id="0" >

              <?php $ctr = 1;  $ctr2 = 0;?>

            @foreach ($dam as $arrays )

             
                <td name='ID' hidden>
                  <input type="text" name='dam_id[]' value="{{$arrays->damid}}" class="form-control"/>
                </td>
                <td name='damname' >
                  <input type="text" name='DamName[]' readonly value="{{$arrays->damName}}" class="form-control"/>
                </td>
              <td name="obserDT">
                <input type="text" name='observation_date[]'  readonly class="form-control" id="observation_date{{ $ctr }}" />
                <input type="text" name='observation_time[]' readonly class="form-control" id="observation_time{{ $ctr }}"/>
                  <input type="text" name='obs_datetime_prev[]' value="{{$arrays->observation_datetime}}" readonly  class="form-control"/>
              </td>
              <td name="RWL">
                <input type="number" step=".01" name='waterlevel_m[]' id= "waterlevel_m_new{{$ctr2}}"  class="form-control"/>
                <input type="text" name='RWL_prev[]' id= "waterlevel_m_old{{$ctr2}}" value="{{$arrays->waterlevel_m}}" readonly=""  class="form-control"/>
              </td>
              <td name="24HRDev">
                  <input type="number" step=".01" name='waterlevel_deviation_m[]' id="deviation{{$ctr2}}"  readonly class="form-control" />  
              </td>
              <td name="NHWL">
                  <input type="number" step=".01" name='norwal_high_water_level[]'  class="form-control"/>
              </td>
              <td name="devFrNHWL">
                  <input type="number" step=".01" name='deviation_fr_nhwl[]'  class="form-control"/>
                  <input type="text" name='devFrNHWL_prev[]' value="{{$arrays->deviation_fr_nhwl}}" readonly  class="form-control"/>
              </td>
              <td name="ruleCurlElev">
                  <input type="number" step=".01" name='rule_curve_elevation[]'  class="form-control"/>
                  <input type="text" name='ruleCurlElev_prev[]' value="{{$arrays->rule_curve_elevation}}" readonly class="form-control"/>
              </td>
              <td name="devFrRuleC">
                  <input type="number" step=".01" name='deviation_from_rule_curve[]'  class="form-control"/>
                  <input type="text" name='devFrRuleC_prev[]' value="{{$arrays->deviation_from_rule_curve}}" readonly   class="form-control"/>
              </td>
              <td name="estRRAmt">
                  <input type="number" step=".01" name='estimated_rr_to_reach_nhwl[]'  class="form-control" />
                  <input type="text" name='estRRAmt_prev[]' value="{{$arrays->estimated_rr_to_reach_nhwl}}"  readonly   class="form-control"/>
              </td>
              <td name="gateOpen">
                  <input type="text"  name='gate_opening[]'  class="form-control"/>
              </td>
            </tr>

            <?php $ctr2 = $ctr2 + 1 ?>

                   

            <?php $ctr = $ctr + 1 ?>

          @endforeach
          </tbody>
        </table>
        <br>
        <br>
        <table align="center">
          <tr>
            <td>Trend for the past 24 hours:</td>
            <td> &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp  &nbsp</td>
            <td>Data Source: Dam Operating/Managing Agencies</td>
          </tr>
          <tr>
            <td>+ Deviation indicates increase from previous WL</td>
          </tr>
          <tr>
            <td>- Deviation indicates decrease from previous WL</td>
          </tr>
        </table>
          <br>
        <br>

      </div>
    </div>
  </div>
</form>
  
</body>


</html>