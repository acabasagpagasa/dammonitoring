<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDamModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dam_monitoring', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('dam_id');
            $table->datetime ('observation_datetime');
            $table->decimal('waterlevel_m',15,2);
            $table->decimal('waterlevel_deviation_m',15,2);
            $table->decimal('norwal_high_water_level',15,2);      
            $table->decimal('deviation_fr_nhwl',15,2); 
            $table->decimal('rule_curve_elevation',15,2);
            $table->decimal('deviation_from_rule_curve',15,2);
            $table->decimal('estimated_rr_to_reach_nhwl',15,2);
            $table->string('gate_opening'); 
            $table->string('date_time_updated');
            $table->string('prepared_by');
            $table->datetime('deleted_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dam_monitoring');
    }
}
