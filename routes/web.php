<?php

use App\DamModel;


Route::get('/create','DamController@funcGetDams');
Route::get('/dams/view/{datetime}','DamController@viewDamStatus');
Route::get('/dams/update/{datetime}','DamController@updateDamStatus');
Route::post('/save','DamController@saveDams');
Route::post('/update','DamController@updateDams');
//Route::get('/lists','DamController@showList');
Route::get('/view/{date}','DamController@viewRec');
Route::get('/update/{date}','DamController@updateRec');
Route::post('/dams/delete','DamController@deleteRec');
Route::get('/login', function () {
    return view('login');
});
Route::get('/login/a', function () {
    return 'haha';
});

Auth::routes();

Route::get('/lists', 'HomeController@index');
