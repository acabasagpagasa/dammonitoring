<?php

namespace App\Http\Controllers;

use Illuminate\Http;
use DB;
use Request;
use App\DamModel;
use App\dams_archive;
use DateTime;
use Redirect;
use Session;
use Illuminate\Database\QueryException;

class DamController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function funcGetDams(){

        $arr = DamModel::select('dams.damName','dams.id as damid','dam_monitoring.observation_datetime', 'dam_monitoring.waterlevel_m', 'dam_monitoring.deviation_fr_nhwl', 'dam_monitoring.rule_curve_elevation', 'dam_monitoring.deviation_from_rule_curve', 'dam_monitoring.estimated_rr_to_reach_nhwl','dam_monitoring.deleted_at')
            ->rightJoin('dams','dam_monitoring.dam_id','=','dams.id')
            ->where('dam_monitoring.deleted_at','=',null)
            ->take(9)
            ->orderBy('observation_datetime','desc')
            ->orderBy('dams.id','asc')
            ->get();

        $time = array('01:00 AM','02:00 AM','03:00 AM','04:00 AM','05:00 AM','06:00 AM','07:00 AM','08:00 AM','09:00 AM','10:00 AM','11:00 AM','12:00 AM','01:00 PM','02:00 PM','03:00 PM','04:00 PM','05:00 PM','06:00 PM','07:00 PM','08:00 PM','09:00 PM','10:00 PM','11:00 PM','12:00 PM');

        $time_u = array();

        foreach ($time as $value) {
            $hours = date('H:i:s',strtotime($value));
            $today = date('Y-m-d').' '.$hours;
            $today1 = date('Y-m-d');

            $vhours = DamModel::select('observation_datetime')
                    ->where('observation_datetime','=', $today)
                    ->count();

                    if($vhours <= 0){
                      array_push($time_u,$value);                      
                    } 
       
        }

    	return view('create',array('dam'=>$arr,'time'=>$time_u));

    }


    public function saveDams(Request $request){
        $data = Request::except('_token','DamName');

        $i = 0;

        foreach ($data['dam_id'] as $d) { 

                $dams = new DamModel; 
                $observation_datetime = new DateTime($data['date_updated'][0] . ' ' . $data['time_updated'][0]);
                $observation_datetime = $observation_datetime->format('Y-m-d H:i:s');


                $dams->dam_id = $data['dam_id'][$i];
                $dams->observation_datetime = $observation_datetime;
                $dams->waterlevel_m = $data['waterlevel_m'][$i];
                $dams->waterlevel_deviation_m = $data['waterlevel_deviation_m'][$i];
                $dams->norwal_high_water_level = $data['norwal_high_water_level'][$i];
                $dams->deviation_fr_nhwl = $data['deviation_fr_nhwl'][$i];
                $dams->rule_curve_elevation = $data['rule_curve_elevation'][$i];
                $dams->deviation_from_rule_curve = $data['deviation_from_rule_curve'][$i];
                $dams->estimated_rr_to_reach_nhwl = $data['estimated_rr_to_reach_nhwl'][$i];
                $dams->gate_opening = $data['gate_opening'][$i];
                $dams->prepared_by = $data['prepared_by'][0];

                $dams->save();

                $i = $i + 1;

                if($i == count($data) - 3){
                    break;
                }

        }
        Session::flash('create','Successfully added.');
        return redirect('/lists');
    }

    public function showList(){

        $mylist = DamModel::select('observation_datetime')
                ->where('deleted_at','=',null)
                ->groupBy('observation_datetime')
                ->orderBy('observation_datetime','desc')

                ->get();

        return view('lists',array('data'=>$mylist));
    }

    public function viewDamStatus($datetime){

        $myRec = DamModel::join('dams','dams.id', '=', 'dam_monitoring.dam_id')
                ->where('dam_monitoring.observation_datetime','=',$datetime)
                ->whereNull('deleted_at')
                ->get();

        return view('view',array('viewRec'=>$myRec));
    }

    public function updateDamStatus($datetime){
        //get prev data
        $data = DB::select(' select max(observation_datetime) as observation_datetime
                 from dam_monitoring where deleted_at is null
                 and  (observation_datetime < (select max(observation_datetime) from dam_monitoring where deleted_at is null)) ');

        $updateRec = DamModel::select('dam_monitoring.*','dams.damName as damName')
                ->join('dams','dams.id', '=', 'dam_monitoring.dam_id')
                ->where('dam_monitoring.observation_datetime','=',$data[0]->observation_datetime)
                ->orderBy('dam_monitoring.observation_datetime','desc')
                ->orderBy('dams.id','asc')
                ->get();

        //get selected data
        $data2 = DamModel::select(DB::raw('max(observation_datetime) as observation_datetime'))
                ->get();


        $rec = DB::select('select a.*,b.damName as damName from dam_monitoring a inner join dams b on a.dam_id = b.id where observation_datetime = ((select max(observation_datetime) as observation_datetime from dam_monitoring where deleted_at is null)) and deleted_at is null');


        $time_u = array('01:00 AM','02:00 AM','03:00 AM','04:00 AM','05:00 AM','06:00 AM','07:00 AM','08:00 AM','09:00 AM','10:00 AM','11:00 AM','12:00 AM','01:00 PM','02:00 PM','03:00 PM','04:00 PM','05:00 PM','06:00 PM','07:00 PM','08:00 PM','09:00 PM','10:00 PM','11:00 PM','12:00 PM');

        $new_time_u = array();

        foreach ($time_u as $value_u) {
            $hours_u = date('H:i:s',strtotime($value_u));
            $today_u = date('Y-m-d').' '.$hours_u;
            
            $vhours_u = DamModel::select('observation_datetime')
                    ->where('observation_datetime','=', $today_u)
                    ->count();

                    if($vhours_u <= 0){
                      array_push($new_time_u,$value_u);                      
                    } 
         
        }

        if ($data[0]->observation_datetime == null) {
            return view('update_first',array('d2'=>$rec,'time'=>$new_time_u));
        } else {
            return view('update',array('updateRec'=>$updateRec,'d2'=>$rec,'time'=>$new_time_u));
        } 
        
    }

    public function updateDams(Request $request){

        $udata = Request::except('_token','DamName');
        $i = 0;

        foreach ($udata['dam_id_mon'] as $d) { 

                $udams =  DamModel::find($d);

                if (is_array($udata['date_updated'])) {
                    $observation_datetime_u = $udata['date_updated'][0] . ' ' . $udata['time_updated'][0];
                } else {
                    
                    $observation_datetime_u = $udata['date_updated'] . ' ' . $udata['time_updated'];
                }
                  

                $datetime = date('Y-m-d H:i:s',strtotime($observation_datetime_u)); 
                $udams->observation_datetime = $datetime;

                $udams->waterlevel_m = $udata['waterlevel_m'][$i];
                $udams->waterlevel_deviation_m = $udata['waterlevel_deviation_m'][$i];
                $udams->norwal_high_water_level = $udata['norwal_high_water_level'][$i];
                $udams->deviation_fr_nhwl = $udata['deviation_fr_nhwl'][$i];
                $udams->rule_curve_elevation = $udata['rule_curve_elevation'][$i];
                $udams->deviation_from_rule_curve = $udata['deviation_from_rule_curve'][$i];
                $udams->estimated_rr_to_reach_nhwl = $udata['estimated_rr_to_reach_nhwl'][$i];
                $udams->gate_opening = $udata['gate_opening'][$i];
                $udams->prepared_by = $udata['prepared_by'];
                $udams->save();

                $i = $i + 1;

                if($i == count($udata) - 3){
                    break;
                }

        }
        Session::flash('update','Successfully updated.');
        return redirect('/lists');
        
    }

    public function deleteRec(){
        $date = Request::input('date_time');

        $record_d = DamModel::where('observation_datetime','=',$date)
                ->get();
               
        $x = 0;
        foreach ($record_d as $del) { 

                $archive = new dams_archive;

                $archive->dam_id = $del->dam_id;
                $archive->observation_datetime = $del->observation_datetime;
                $archive->waterlevel_m = $del->waterlevel_m;
                $archive->waterlevel_deviation_m = $del->waterlevel_deviation_m;
                $archive->norwal_high_water_level = $del->norwal_high_water_level;
                $archive->deviation_fr_nhwl = $del->deviation_fr_nhwl;
                $archive->rule_curve_elevation = $del->rule_curve_elevation;
                $archive->deviation_from_rule_curve = $del->deviation_from_rule_curve;
                $archive->estimated_rr_to_reach_nhwl = $del->estimated_rr_to_reach_nhwl;
                $archive->gate_opening = $del->gate_opening;
                $archive->prepared_by = $del->prepared_by;
                $archive->deleted_at = date('Y-m-d H:i:s');
                $archive->created_at = $del->created_at;
                $archive->updated_at = $del->updated_at;

                $archive->save();

                $x = $x + 1;

                if($x == count($record_d)){
                    break;
                }

        }

        DamModel::where('observation_datetime',$date)->delete();

        Session::flash('success','Successfully deleted.');
        return redirect::back();
        
    }

}