<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dams_archive extends Model
{
    protected $primarykey = 'id';
	protected $table = 'dam_monitoring_archive';
	protected $fillables = [
            'dam_id',
            'observation_datetime',
            'waterlevel_m',
            'waterlevel_deviation_m',
            'norwal_high_water_level',
            'deviation_fr_nhwl', 
            'rule_curve_elevation',
            'deviation_from_rule_curve',
            'estimated_rr_to_reach_nhwl',
            'gate_opening', 
            'prepared_by',
            'deleted_at',
            'created_at',
            'updated_at'
	];
}
